<!---Page Head Begin--->
<?php include_once('page_head.php'); ?>
<!---Page Head End--->

<body>

<!---Page Header Begin--->
<div class="page-header">

    <?php include_once('page_header_top.php'); ?>

    <?php include_once('page_menu.php'); ?>

</div>

<!---End of Page Header--->


<!---Page Content Begin--->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Dashboard <small>statistics & reports</small></h1>
			</div>
			<!-- END PAGE TITLE -->

		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">

		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!---Page Content End--->


<!---Page Footer Begin--->
<?php include_once('page_footer.php'); ?>
<!---Page Footer End--->